name := "project"

version := "0.1"

scalaVersion := "2.12.4"

scalacOptions ++= Seq("-Ypartial-unification")

resolvers += Resolver.sonatypeRepo("snapshots")

val http4sVersion = "0.18.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "co.fs2" %% "fs2-core" % "0.10.0-M8",
  "org.slf4j" % "slf4j-simple" % "1.7.25"
)
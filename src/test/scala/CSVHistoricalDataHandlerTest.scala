import CSVHistoricalDataHandler._
import org.scalatest.{BeforeAndAfterEach, FlatSpec}

import scala.io.Source.fromURL

class CSVHistoricalDataHandlerTest extends FlatSpec with BeforeAndAfterEach {

  behavior of "CSVHistoricalDataHandlerTest"

  private val tickers = Array[String]("GOOG", "BIDU", "YNDX")
  private val urls: Array[String] = tickers.map(url + _)
  private var targets: Array[Vector[Double]] = _

  override protected def beforeEach(): Unit = {
    targets = urls.map { url =>
      fromURL(url)
        .getLines
        .drop(1)
        .map(_.split(","))
        .map(_ (columns("Close")).toDouble)
        .toVector
    }
  }

  it should "get vector of prices for a given ticker" in {
    tickers.indices.foreach { index =>
      assert(targets(index) === prices(tickers(index)).unsafeRunSync())
    }
  }

  it should "get vector of profit for a given ticker" in {
    val profits = targets
      .map(v => v.init zip v.tail)
      .map(_.map(day => (day._1 - day._2) / day._2))

    tickers.indices.foreach { index =>
      assert(profits(index) === profit(tickers(index)).unsafeRunSync())
    }
  }

  it should "get vector of a single average price value for a given ticker" in {
    tickers.indices.foreach { index =>
      val tup = targets(index)
        .map((_, 1))
        .reduceLeft((a, b) => (a._1 + b._1, a._2 + b._2))

      val target = Vector[Double](tup._1 / tup._2)
      val result = avgPrice(tickers(index)).unsafeRunSync()

      assert(target.head === result.head)
      assert(target.tail === result.tail)
      assert(Vector() === result.tail)
    }
  }

  it should "get vector of a single average profit value for a given ticker" in {
    val avgProfits = targets
      .map(v => v.init zip v.tail)
      .map(_.map(day => (day._1 - day._2) / day._2))
      .map(_.map((_, 1)).reduceLeft((a, b) => (a._1 + b._1, a._2 + b._2)))
      .map(tup => Vector[Double](tup._1 / tup._2))

    tickers.indices.foreach { index =>

      val target = avgProfits(index)
      val result = avgProfit(tickers(index)).unsafeRunSync()

      assert(target.head === result.head)
      assert(target.tail === result.tail)
      assert(Vector() === result.tail)
    }
  }
}

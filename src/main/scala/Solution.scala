/*
* Написать функции, которые по тикеру запрашивают исторические
* данные с https://www.google.com/finance/historical?q=NASDAQ:$ticker&output=csv и выдают данные по Close price -
* соответственно список цен за последний год (дата последней цены - 365 дней),
* список значений прибыли за последний год (разница цены с предыдущим днем деленная на цену предыдущего дня),
* средняя цена за весь период и
* средняя прибыль за весь период
* итого 4 функции
* $ticker - это параметр
* Например GOOG
*/

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import cats.effect.IO
import fs2.text
import org.http4s.client.Client
import org.http4s.client.blaze._

/** Utility object with functions to get prices and profit for a given ticker.
  * It uses [[org.http4s.client.blaze.PooledHttp1Client]] to send get requests
  * as IO[String] which is used for a construction of [[fs2.Stream]] with lazy evaluation
  * and to perform a connection close method provided by bracket function of fs2.Stream.
  * Helper functionality is divided into small helper functions, which are private.
  */
object Solution {
  // default url without ticker parameter
  private val url: String = s"http://finance.google.com/finance/historical?output=csv&q=NASDAQ:"

  /**
    * Entry point to a program to see results or to work with command line arguments.
    * Prints a list of prices, profits and average value for both
    */
  def main(args: Array[String]): Unit = {
    val ticker = args match {
      case arr: Array[String] if arr.isEmpty => "GOOG"
      case _ => args.head
    }
    println(s"\nPrices of 'Close' column for a '$ticker' ticker\n".toUpperCase)
    val price = prices(ticker)
    price.foreach(println)
    val (to, from, avg) = avgPrice(price)
    println(s"\nAverage price from $from to $to is $avg\n".toUpperCase)

    println(s"\n${"===================" * 9}\n")

    println(s"\n\nProfit of 'Close' column for a '$ticker' ticker\n\n".toUpperCase)
    val profi = profit(ticker)
    profi.foreach(println)
    val (to2, from2, avg2) = avgProfit(profi)
    println(s"\nAverage profit from $from to $to is $avg2\n".toUpperCase)
  }

  /** Function to get prices for given ticker
    *
    * @param ticker string with param for a url
    * @param period in days (default 365)
    * @param column name with Double values (default "Close")
    * @return Vector of tuples (date: LocalDate, price: Double)
    */
  def prices(ticker: String,
             period: Int = 365,
             column: String = "Close"): Vector[(LocalDate, Double)] = {
    val (content, index, date) = resolve(ticker, period, column)
    content.tail // drop csv header
      .map(row => (toLocalDate(row.head), row(index).toDouble))
      .takeWhile(_._1.isAfter(date)) // _._1 is a LocalDate of a row
  }

  /** Function to get average value for price, uses get request
    *
    * @param ticker string with param for a url
    * @param period in days (default 365)
    * @param column name with Double values (default "Close")
    * @return Tuple3 with (from: LocalDate, to: LocalDate, average: Double)
    */
  def avgPrice(ticker: String,
               period: Int = 365,
               column: String = "Close"): (LocalDate, LocalDate, Double) = {
    avgPrice(prices(ticker, period, column))
  }

  /** Helper private function to get average value for prices, does not use get request
    * for a testing purpose in the main method, can be public to prevent send request twice
    * if the sequence of prices is already known
    *
    * @param result a sequence of Tuple2 where (date: LocalDate, price: Double)
    * @return Tuple3 with (from: LocalDate, to: LocalDate, average: Double)
    */
  private def avgPrice(result: Seq[(LocalDate, Double)]): (LocalDate, LocalDate, Double) = {
    (result.head._1, result.last._1, result.map(_._2).sum / result.length)
  }

  /** Function to get profit list of difference between two days in given period
    *
    * @param ticker string with param for a url
    * @param period in days (default 365)
    * @param column name with Double values (default "Close")
    * @return Vectorof Tuple3 where (day: LocalDate, dayBefore: LocalDate, profitDifference: Double)
    */
  def profit(ticker: String,
             period: Int = 365,
             column: String = "Close"): Vector[(LocalDate, LocalDate, Double)] = {
    val result: Vector[(LocalDate, Double)] = prices(ticker, period, column)
    (result.init zip result.tail) // zip day with day before
      .map { day =>
      val difference = (day._1._2 - day._2._2) / day._2._2
      (day._1._1, day._2._1, difference)
    }
  }

  /** Function to get average value for profit, uses get request
    *
    * @param ticker string with param for a url
    * @param period in days (default 365)
    * @param column name with Double values (default "Close")
    * @return Tuple3 with (from: LocalDate, to: LocalDate, average: Double)
    */
  def avgProfit(ticker: String,
                period: Int = 365,
                column: String = "Close"): (LocalDate, LocalDate, Double) = {
    avgProfit(profit(ticker, period, column))
  }

  /** Helper private function to get average value for profits, does not use get request
    * for a testing purpose in the main method, can be public to prevent send request twice
    * if the sequence of profit is already known
    *
    * @param result a sequence of Tuple3 where (day: LocalDate, dayBefore: LocalDate, price: Double)
    * @return Tuple3 with (from: LocalDate, to: LocalDate, average: Double)
    */
  private def avgProfit(result: Seq[(LocalDate, LocalDate, Double)]): (LocalDate, LocalDate, Double) = {
    (result.head._1, result.last._2, result.map(_._3).sum / result.length)
  }

  /** Helper private function to get csv content from given address
    * Uses fs2.Stream.bracket method to shutdown httpClient after finishing
    *
    * @param url String with full url to retrieve csv file
    * @return fs2.Stream[IO, Array[String](rows)] a lazy stream with IO to send
    *         Get request, and then close http client and content of splitted
    *         rows in scv file, including header
    */
  private def csvContent(url: String): fs2.Stream[IO, Array[String]] = {
    val request: Client[IO] = PooledHttp1Client[IO]()
    val acquire: IO[String] = request.expect[String](url)
    val release: IO[Unit] = IO(request.shutdownNow)
    fs2.Stream
      .bracket(acquire)(fs2.Stream(_), _ => release)
      .through(text.lines)
      .filter(_.nonEmpty)
      .map(_.trim.split(","))
  }

  /** Helper private function to get csv content, index for a column and toDate point for a period
    *
    * @param ticker string with param for a url
    * @param period in days
    * @param column name with Double values
    * @return Tuple3 of (csvContent: Vector[Array[String](rows)], indexOfCSVColumn: Int, endDatePeriod: LocalDate)
    */
  private def resolve(ticker: String,
                      period: Int,
                      column: String) = {
    val content: Vector[Array[String]] = csvContent(s"$url$ticker").runLog.unsafeRunSync
    val index: Int = content.head.indexOf(column)
    val date: LocalDate = toLocalDate(content.tail.head.head).minusDays(period) // or .minusYears(1)
    (content, index, date)
  }

  private val formatter = DateTimeFormatter.ofPattern("dd-MMM-yy")

  /** Helper function for Date formatting of pattern dd-MMM-yy
    *
    * @param date a string with pattern e.g. 18-Nov-17
    * @return LocalDate object from parsed string
    */
  private def toLocalDate(date: String): LocalDate = {
    date.length match {
      case 9 => LocalDate.parse(date, formatter)
      case _ => LocalDate.parse(0 + date, formatter) // due to 8-Nov-17 format, to get 08-Nov-17 for formatter
    }
  }
}

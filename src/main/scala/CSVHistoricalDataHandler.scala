import cats.effect.IO
import fs2.Stream
import fs2.text.lines
import org.http4s.client._
import org.http4s.client.blaze._

/** Utility object with functions to get prices and profit by given ticker */
object CSVHistoricalDataHandler {
  val url = s"http://finance.google.com/finance/historical?output=csv&q=NASDAQ:"
  val columns = Map("Open" -> 1, "High" -> 2, "Low" -> 3, "Close" -> 4)

  /** Function to get prices for given ticker
    *
    * @param ticker string with param for url
    * @return `IO[Vector[Double]]` effect with Vector of price values
    */
  def prices(ticker: String): IO[Vector[Double]] = {
    stream(ticker).runLog
  }

  /** Function to get profit by given ticker
    *
    * @param ticker string with param for url
    * @return `IO[Vector[Double]]` effect with Vector of profit values
    */
  def profit(ticker: String): IO[Vector[Double]] = {
    stream(ticker)
      .sliding(2)
      .map(q => (q.head, q.tail.head))
      .map(day => (day._1 - day._2) / day._2)
      .runLog
  }

  /** Function to get average value of price by given ticker
    *
    * @param ticker string with param for url
    * @return `IO[Vector[Double]]` effect with Vector of a single value (average price)
    */
  def avgPrice(ticker: String): IO[Vector[Double]] = {
    stream(ticker)
      .map((_, 1))
      .reduce((a, b) => (a._1 + b._1, a._2 + b._2))
      .map(p => p._1 / p._2)
      .runLog
  }

  /** Function to get average value of profit by given ticker
    *
    * @param ticker string as param for url
    * @return `IO[Vector[Double]]` effect with Vector of a single value (average profit)
    */
  def avgProfit(ticker: String): IO[Vector[Double]] = {
    stream(ticker)
      .sliding(2)
      .map(q => (q.head, q.tail.head))
      .map(day => (day._1 - day._2) / day._2)
      .map((_, 1))
      .reduce((a, b) => (a._1 + b._1, a._2 + b._2))
      .map(p => p._1 / p._2)
      .runLog
  }

  /** Helper private function to obtain a fs2.Stream of csv row
    *
    * @param ticker string as param for url
    * @param column an index to obtain column from csv (default "Close" -> 4)
    * @return Stream[IO, Double] stream of effect and result Double values
    */
  private def stream(ticker: String, column: Int = columns("Close")): Stream[IO, Double] = {
    val (acquire, release) = request(s"$url$ticker")
    Stream.eval(acquire)
      .onFinalize(release)
      .through(lines)
      .filter(_.nonEmpty)
      .drop(1)
      .map(_.trim.split(","))
      .map(_ (column).toDouble)
  }

  /** Helper private function to send get request
    *
    * @param url string to send request
    * @return Tuple2 of effects (IO[responseBody], IO[shutdownClient]))
    */
  private def request(url: String): (IO[String], IO[Unit]) = {
    val request: Client[IO] = PooledHttp1Client[IO]()
    val acquire: IO[String] = request.expect[String](url)
    val release: IO[Unit] = IO(request.shutdownNow)
    (acquire, release)
  }
}
